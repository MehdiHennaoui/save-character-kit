import {ROUTES} from "../../src/services/constants/routes";

describe("Login test", function () {
  const urlLogin = ROUTES.LOGIN;
  it("Navigate to Login page", function () {
    cy.visit('/')
      .get(`a[href='${urlLogin}']`)
      .click()
      .url()
      .should('include', urlLogin)
  });
  it("should be type email@email.com in email input", function () {
    const emailGood = "email@email.com";
    cy.get('input[name=email]').type(emailGood).should('have.value', emailGood);
  })
});
