import {FormProvider, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {createUserWithEmailAndPassword, updateProfile} from "firebase/auth";
import {subsribeValidationSchema} from "../services/form/validation";
import {InputMail, InputPassword, InputText} from "../component/form";
import {ButtonPrimary} from "../component/buttons";
import {auth} from "../services/network/firebaseConfig";
import {IS_DEV} from "../services/utils";
import {useStoreActions} from "../services/store";
import {useNavigate} from "react-router";
import {ROUTES} from "../services/constants/routes";

export const Subscribe = () => {
  return (
    <>
      <h1 className={'text-xl'}>Subscribe</h1>
      <Form />
    </>
  )
}

interface FormValues {
  email: string;
  name: string;
  password: string;
  "password-confirm": string;
}

const defaultValues: FormValues | {} = IS_DEV ? {
  email: "email14@email.com",
  name: "jondoe",
  password: "123456",
  "password-confirm": "123456",
} : {}

const Form = () => {
  const methods = useForm({resolver: yupResolver(subsribeValidationSchema), defaultValues})
  const addUser = useStoreActions((actions) => {
    return actions.addUser;
  });
  const navigate = useNavigate();

  const onSubmit = async (data: FormValues) => {
    const {email, password, name} = data;
    try {
      const {user} = await createUserWithEmailAndPassword(auth, email, password);
      if(user) {
        const userInfo : any = await auth.currentUser;
        await updateProfile(userInfo, {
          displayName: name,
        })
        addUser(user);
        navigate(ROUTES.APP);
      }
    } catch (error) {
      console.error("subscribe on submit error ", error);
    }
  };

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <InputMail name={"email"} label={"Email"} />
        <InputText name={"name"} label={"Pseudo"} />
        <InputPassword name={"password"} label={"Mot de passe"}/>
        <InputPassword name={"password-confirm"} label={"Confirmation de mot de passe"} />
        <ButtonPrimary className={"flex ml-auto mt-2 mr-2"} type={"submit"} text={"Envoyer"} />
      </form>
    </FormProvider>
  );
}
