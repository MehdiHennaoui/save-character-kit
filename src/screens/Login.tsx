import {useForm, FormProvider} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import {useNavigate} from "react-router-dom";
import {signInWithEmailAndPassword} from "firebase/auth";
import {InputMail, InputPassword} from "../component/form";
import {ButtonPrimary} from "../component/buttons";
import {loginValidationSchema} from "../services/form/validation";
import {formLoginNames} from "../services/form/names";
import {IS_DEV} from "../services/utils";
import {auth} from "../services/network/firebaseConfig";
import {useStoreActions} from "../services/store";
import {ROUTES} from "../services/constants/routes";

export const Login = () => {
  return (
    <>
      <h1 className={"text-xl"}>Login</h1>
      <Form />
    </>
  )
}

interface FormValues {
  [formLoginNames.email]: string;
  [formLoginNames.password]: string;
}

const defaultValues = IS_DEV ? {[formLoginNames.email]: "email14@email.com", [formLoginNames.password]: "123456"} : {}

const Form = () => {
  const methods = useForm({resolver: yupResolver(loginValidationSchema), defaultValues})
  const addUser = useStoreActions((actions) => actions.addUser)
  const navigate = useNavigate();

  const onSubmit = async (data: FormValues) => {
    const {email, password} = data;
    try {
        const { user } = await signInWithEmailAndPassword(auth, email, password);
        addUser(user);
        navigate(ROUTES.APP, {replace: true});
     } catch (e) {
      console.error()
    }
  };

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <div className={"flex flex-col"}>
          <InputMail label={"Email"} name={formLoginNames.email} />
          <InputPassword label={"Mot de passe"} name={formLoginNames.password} />
        </div>
        <ButtonPrimary className={"flex ml-auto mt-2 mr-2"} type={"submit"} text={"Envoyer"} />
      </form>
    </FormProvider>
  )
}
