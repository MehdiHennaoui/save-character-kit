import React from "react";
import {getUserName} from "../services/models/user";
import {useStoreState} from "easy-peasy";
import {LinkSimple} from "../component/common";
import {ROUTES} from "../services/constants/routes";

const App = () => {
  const userName = useStoreState(getUserName);
  return (
    <div>
      <h1>{userName}</h1>
      <LinkSimple path={ROUTES.CHARACTER_ADD} text={"ajouter un personnage"} />
    </div>
  )
}

export default App;
