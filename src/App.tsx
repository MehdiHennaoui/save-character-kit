import {Navigation} from "./services/navigation";
import {StoreProvider} from "easy-peasy";
import {store} from "./services/store";

function App() {
  return (
    <StoreProvider store={store}>
      <Navigation />
    </StoreProvider>
  );
}

export default App;
