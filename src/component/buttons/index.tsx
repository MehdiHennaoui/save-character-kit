import React, {ComponentProps, FC, HTMLProps} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconProp} from "@fortawesome/fontawesome-svg-core";

interface ButtonProps extends ComponentProps<"button"> {
  text: string,
  className?: string,
}

const Button: FC<ButtonProps> = ({onClick, text, type, className}) => {

  return (
    <button type={type} className={`p-3 rounded-xl ${className}`} onClick={onClick}>
      <div>{text}</div>
    </button>
  )
}

export const ButtonPrimary: FC<ButtonProps> = ({onClick, text, className, type}) => {
  return <Button type={type} className={`bg-green-500 ${className}`} text={text} onClick={onClick} />
}

interface ButtonIconProps extends HTMLProps<HTMLButtonElement> {
  icon: IconProp,
  className?: string,
}

export const ButtonIcon: FC<ButtonIconProps> = ({onClick, icon, className}) => {
  return (
    <button onClick={onClick} className={className}>
      <FontAwesomeIcon icon={icon} />
    </button>
  );
}
