import React, {FC, ReactNode} from "react";
import {ROUTES} from "../../services/constants/routes";
import {Link} from "react-router-dom";

interface MainContainerProps {
  children: ReactNode,
}

export const MainContainer: FC<MainContainerProps> = ({children}) => {
  return (
    <main className={"flex flex-col min-h-screen max-w-2xl bg-pink-100 mx-3"}>
      {children}
    </main>
  )
}

type LinkSimpleProps = {
 path: ROUTES,
 text: string,
}

export const LinkSimple: FC<LinkSimpleProps> = ({path, text}) => {
  return <Link to={path} className={"underline-offset-2"}>
    {text}
  </Link>
}


