import React, {useState, FC, ReactNode} from "react";
import {useFormContext} from "react-hook-form";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {ButtonIcon} from "../buttons";

interface InputProps {
  name: string,
  label: string,
  type: string,
  icon?: ReactNode,
}

const Input: FC<InputProps> = ({name, label, type, icon}) => {
  const { register, formState: {errors} } = useFormContext()
  const iconElement = icon ?? icon;
  const errorText = errors?.[name]?.message && <span className={"text-red-600 my-2 ml-3"}>{errors?.[name]?.message}</span>;

  return (
    <div className="flex flex-col mx-2 mb-2">
      <label className={"my-2"} htmlFor={name}>{label}</label>
      <div className="relative">
        <input className="p-2 border-2 rounded-xl border-gray-900 w-full" {...register(name)} id={name} type={type} />
        {iconElement}
      </div>
      {errorText}
    </div>
  )
}

interface InputContentProps {
  name: string,
  label: string,
}

export const InputText: FC<InputContentProps> = ({name, label}) => {
  return <Input name={name} label={label} type={"text"} />
};

export const InputMail: FC<InputContentProps> = ({name, label}) => {
  return <Input name={name} label={label} type={"email"} />
};

interface InputPasswordProps {
  name: string,
  label: string,
}

export const InputPassword: FC<InputPasswordProps> = ({name, label}) => {
  const [isTextVisible, setIsTextVisible] = useState(false);
  const type = isTextVisible ? "text" : "password";
  const iconPassword = isTextVisible ? faEyeSlash : faEye;

  const toggleIsTextVisible = () => {
    return setIsTextVisible(prevState => {
      return !prevState;
    })
  }

  return <Input
    name={name}
    label={label}
    type={type}
    icon={
      <ButtonIcon
        className={"absolute right-3 bottom-1/2 transform translate-y-1/2"}
        icon={iconPassword}
        onClick={toggleIsTextVisible}/>
    }
  />
};
