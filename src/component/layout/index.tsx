import React, {FC, ReactNode} from "react";
import {NavLink} from "react-router-dom";
import {ROUTES} from "../../services/constants/routes";
import {useStoreActions, useStoreState} from "../../services/store";
import firebase from "firebase";
import {useNavigate} from "react-router-dom";

interface LayoutsProps {
  children: ReactNode,
}

export const Layout: FC<LayoutsProps> = ({children}) => {
  return (
    <div className={"flex flex-col min-h-screen"}>
      <Header />
      <main className={"flex flex-1 flex-col max-w-2xl mx-3"}>
        {children}
      </main>
      <footer>
        footer
      </footer>
    </div>
  )
}

interface LinkNavProps {
  text: string,
  url: string,
  onClick?: () => void,
}

const LinkNav: FC<LinkNavProps> = ({text, url, onClick= () => {}}) => {
  return (
    <li className={"flex-1"}>
      <NavLink onClick={onClick} className={({isActive}) => `block p-2 text-center ${isActive ? "font-bold" : ""}`} to={url}>{text}</NavLink>
    </li>
  )
}


const Header = () => {
  const isLoggedIn = useStoreState(state => {
    return state.isLoggedIn;
  });

  return (
    <nav className={"mb-5 shadow-md"}>
      <ul className={"grid grid-cols-4"}>
        <Links isLoggedIn={isLoggedIn} />
      </ul>
    </nav>
  )
}

// @ts-ignore
const Links = ({isLoggedIn}) => {
  const navigate = useNavigate();
  const removeUser = useStoreActions(actions => actions.removeUser);

  const disconnect = async () => {
    try {
      const result = await firebase.auth().signOut();
      removeUser();
      navigate(ROUTES.LOGIN);
      console.log('result ', result);
    }
    catch (e) {
      console.error("disconnect error ", e);
    }
  };
  console.log("login ", isLoggedIn)

  if(!isLoggedIn) {
    return (
      <>
        <LinkNav url={ROUTES.HOME} text={"logo"} />
        <LinkNav url={ROUTES.HOME} text={"home"} />
        <LinkNav url={ROUTES.LOGIN} text={"login"} />
        <LinkNav url={ROUTES.SUBSCRIBE} text={"subscribe"} />
      </>
    )
  }
  return (
    <>
      <LinkNav url={ROUTES.HOME} text={"logo"} />
      <LinkNav url={ROUTES.HOME} text={"home"} />
      <LinkNav url={ROUTES.APP} text={"app"} />
      <LinkNav url={ROUTES.LOGIN} text={"disconnect"} onClick={disconnect}/>
    </>
  );
}
