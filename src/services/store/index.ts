import {createStore, action, Action, Computed, computed, createTypedHooks} from "easy-peasy";
import firebase from "firebase/auth";
type User = firebase.User|null;

export interface StoreModel {
  user: User,
  addUser: Action<StoreModel, User>,
  removeUser: Action<StoreModel>,
  isLoggedIn: Computed<StoreModel, boolean>
}

export const store = createStore<StoreModel>({
  user: null,
  addUser: action((state, payload) => {
    state.user = payload;
  }),
  removeUser: action((state) => {
    state.user = null;
  }),
  isLoggedIn: computed(state => state.user !== null),
});

const typeHooks = createTypedHooks<StoreModel>();

export const useStoreActions = typeHooks.useStoreActions;
export const useStoreDispatch = typeHooks.useStoreDispatch;
export const useStoreState = typeHooks.useStoreState;
