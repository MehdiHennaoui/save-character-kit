export enum ROUTES  {
  HOME = "/",
  LOGIN = "/login",
  SUBSCRIBE = "/subscribe",
  APP = "/app",
  CHARACTER_ADD = "/character_add",
}
