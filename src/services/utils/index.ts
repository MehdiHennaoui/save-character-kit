const nodeEnv = process.env.NODE_ENV
export const IS_DEV: boolean = !nodeEnv || nodeEnv === "development";
