import {initializeApp} from 'firebase/app';
import {getAuth} from "firebase/auth";
import {getDatabase} from "firebase/database";

const env = process.env;

const config = {
  apiKey: env.REACT_APP_API_KEY,
  authDomain: env.REACT_APP_AUTH_DOMAIN,
  projectId: env.REACT_APP_PROJECT_ID,
  storageBucket: env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: env.REACT_APP_STORAGE_BUCKET,
  appId: env.REACT_APP_APP_ID,
  databaseURL: env.REACT_APP_DATABASE_URL,
};

initializeApp(config);
export const auth = getAuth();
export const database = getDatabase();
