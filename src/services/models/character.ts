import {ref, set} from "firebase/database";
import {database} from "../network/firebaseConfig";

type Character = {
  id: number,
  idUser: number,
  name: string,
  game: string,
}
export const setCharacter = ({id, idUser, name, game}: Character) => {
  set(ref(database,'characters/' + id), {
    id,
    idUser,
    name,
    game,
  })
}
