import {StoreModel} from "../store";

export const getUser = (state: StoreModel) => {
  return state?.user || {}
}

export const getUserName = (state: StoreModel) => {
  const user = getUser(state);
  // @ts-ignore
  return user?.displayName || "";
}
