import React, {FC} from "react";
import {BrowserRouter as Router, Navigate, Route, Routes, useLocation} from "react-router-dom";
import {Layout} from "../../component/layout";
import {Login} from "../../screens/Login";
import {Home} from "../../screens/Home";
import {Subscribe} from "../../screens/Subscribe";
import {ROUTES} from "../constants/routes";
import App from "../../screens/App";
import {useStoreState} from "../store";
import {CharacterAdd} from "../../screens/CharacterAdd";

export const Navigation = () => {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route
            path={ROUTES.HOME}
            element={<Home/>}/>
          <Route
            path={ROUTES.LOGIN}
            element={<Login/>}/>
          <Route
            path={ROUTES.SUBSCRIBE}
            element={<Subscribe/>}/>
          <Route
            path={ROUTES.APP}
            element={<RequireAuth
                        redirecto={ROUTES.LOGIN}
                        children={<App />} />} />
          <Route
            path={ROUTES.CHARACTER_ADD}
            element={<RequireAuth
                        redirecto={ROUTES.LOGIN}
                        children={<CharacterAdd />} />} />
        </Routes>
      </Layout>
    </Router>
  );
};

type RouteWrapperProps = {
  children: JSX.Element,
  redirecto: ROUTES,
}

const RequireAuth : FC<RouteWrapperProps> = ({children, redirecto}) => {
  const isLoggedIn = useStoreState(state => {
    return state.isLoggedIn;
  })
  const location = useLocation();
  return isLoggedIn ? children : <Navigate to={redirecto} state={{from: location}} replace />;
};
