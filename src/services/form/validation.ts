import * as yup from "yup"
import {formLoginNames, formSubscribeNames} from "./names";

const errors = {
  defined: "ce champ est requis"
}

export const loginValidationSchema = yup.object().shape({
  [formLoginNames.email]: yup.string().email().required(errors.defined),
  [formLoginNames.password]: yup.string().required(errors.defined),
});

export const subsribeValidationSchema = yup.object().shape({
  [formSubscribeNames.email]: yup.string().email().required(errors.defined),
  [formSubscribeNames.name]: yup.string().required(errors.defined),
  [formSubscribeNames.password]: yup.string().min(6).required(errors.defined),
  [formSubscribeNames.passwordConfirm]: yup.string().min(6).required(errors.defined).oneOf([yup.ref(formSubscribeNames.password)]),
});

