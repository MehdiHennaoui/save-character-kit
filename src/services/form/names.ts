export enum formLoginNames {
  email = "email",
  password = "password"
}

export enum formSubscribeNames {
  email = "email",
  name = "name",
  password = "password",
  passwordConfirm = "password-confirm",
}
